
# Write CI-CD Variables in Pipeline

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-10-29

* **GitLab Version Released On**: 13.5

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **Tested On**: 
  * GitLab Docker-Executor Runner using bash:latest container

* **References and Featured In**:

## Demonstrates These Design Requirements, Desirements and AntiPatterns

* **Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Sequential versioning across ALL branches - including multiple development branches.  Compared to the common pattern of storing the version number in a Git based repository file, this method of using a CI CD variable will not generate duplicate version numbers across branches. This is more critical when multiple branches push the resultant packages to the same packaging endpoint. In this case, every branch being simultaneously developed and pushed to a single end point should generate unique version numbers in the same series.
* **Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use a build data variable that must persist across builds (version number in this case).
* **Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Specifically persisting an incremented version number across builds.
* **Development Pattern:** Handy shell script for incrementing semver versions (increment_version.sh)
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Persisted, build modified variables by leveraging Gitlab API to write a variable to a project level.
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Persisted, build modified variables by leveraging Gitlab API to write a variable to a group level.
* **Cross Reference Pattern:** * Example of commiting a file to the repository to store persisted, build modified variables: https://gitlab.com/guided-explorations/language-specific/npm/npm-packaging

### Uses and features for Generic Packages (Released in 13.5):
* Any file can be uploaded and retrieved, including tar / zip archives.
* No built-in expression or resolution of dependencies - because there is no client side package manager utility.
* A Package Manager type that is not yet supported (e.g. Yum)
* GitLab “Releases” functionality is slated use Generic packages as it's storage mechanism in the future.
* Note: Some Package Managers can treat disk files as a repository for dependency resolution (e.g. Yum)- and sometimes you can combine that will actual package endpoints (e.g. Chocolatey NuGet)

### Cross References and Documentation
* Generic Packages Feature Help: https://docs.gitlab.com/ee/user/packages/generic_packages/
* API to write to project variables: https://docs.gitlab.com/ee/api/project_level_variables.html
* API to write group variables: https://docs.gitlab.com/ee/api/group_level_variables.html

#### For generic packages:
* Demonstrates methods for embedding the version meta data - either in the file name or a file inside the archive (for when you are uploading an archive file)
